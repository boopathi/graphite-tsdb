package server

import (
  "graphite/web"
)

func Start () {
  //Initialize the routes first
  routes()
  //Now start the server instance
  web.Run("0.0.0.0:8080")
}

