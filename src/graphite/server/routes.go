package server

import (
  "graphite/web"
)

func routes() {
  //Note : Order matters
  //       First pattern that matches is served
  web.Get("/render",render)
  web.Get("/(.*)",index)
}


