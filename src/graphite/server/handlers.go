package server

import (
  "graphite/web"
  "graphite/mustache"
)
func index(s string) string {
  return mustache.RenderFile("templates/index.html",map[string]string {
    "name": s,
  })
}

func render(ctx *web.Context) string {
  return fetch()
}
